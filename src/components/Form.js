import { Component } from "react";

const onSubmitHandler = () => {
    console.log("Form đã được submit");
}

const onSelectChangeHandler = (event) => {
    console.log(event.target.value);
}
class Form extends Component {
    render() {
        return (
            <>
                <form className="mt-5" onSubmit={onSubmitHandler}>
                    <div className="row">
                        <div className="col-sm-3">
                            First Name
                        </div>
                        <div className="col-sm-9">
                            <input placeholder="Your name" className="form-control" onChange={onSelectChangeHandler} />
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-sm-3">
                            Last Name
                        </div>
                        <div className="col-sm-9">
                            <input placeholder="Your last name" className="form-control" onChange={onSelectChangeHandler} />
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-sm-3">
                            Country
                        </div>
                        <div className="col-sm-9">
                            <select className="form-control" onChange={onSelectChangeHandler}>
                                <option value="AUS">Australia</option>
                                <option value="VN">Việt Nam</option>
                                <option value="USA">Hoa Kỳ</option>
                            </select>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-sm-3">
                            Subject
                        </div>
                        <div className="col-sm-9">
                            <textarea placeholder="Write something..." className="form-control" rows="10" onChange={onSelectChangeHandler} />
                        </div>
                    </div>
                    <div className="mt-3">
                        <div className="col-sm-3">
                            <button type="submit" className="btn btn-success" >
                                Send Data
                            </button>
                        </div>
                    </div>
                </form>
            </>
        )
    }
}

export default Form;