import "bootstrap/dist/css/bootstrap.min.css"
import Form from "./components/Form";
import Title from "./components/Title";

function App() {
  return (
    <div className="container">
      <Title />
      <Form />
    </div>
  );
}

export default App;
